//TODO: Doc and fix naming if necessary
enum FilterType {
  selection, number, toggle
}

class Filter {
  final String id;
  final FilterType type;
  final dynamic value;

  Filter(this.id, this.type, this.value);
}

class FilterMetaData {
  final String id;
  final FilterType type;
  final dynamic options;

  FilterMetaData(this.id, this.type, this.options); 
}