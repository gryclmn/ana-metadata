# ANA internal module for metadata

All documents to be store into the database has to implement `Couchy` or `Propsy`.

`Schema` is there to give a soft data layout for each type of document. It contains `Rule` to define the available field in a type of document and its usage.

`CommonWord` tie a list of text for each `Rule` in the `Schema`. Think of it like input suggestion or drop-down options.

All classes in this package and ANA made strong use of [Built Value](https://github.com/google/built_value.dart). You need to understand this fully to be able to understand how to manipulate data in ANA.

## Note on usage

Usually you do not want to make changes to these files, unless ANA is making fundamental changes to it's data. Any changes in this file will have a cascading effect. It can cause data corruption or missing data if not done carefully.

Any changes in this package must run a `pub run build_runner build` or `pub run build_runner watch` at the end, and commit the generated file to the git repo. This is so that all packages and ANA, who depends on this package, will function correctly.