// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CouchDBDoc extends CouchDBDoc {
  @override
  final String id;
  @override
  final String rev;
  @override
  final bool deleted;

  factory _$CouchDBDoc([void Function(CouchDBDocBuilder) updates]) =>
      (new CouchDBDocBuilder()..update(updates)).build();

  _$CouchDBDoc._({this.id, this.rev, this.deleted}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('CouchDBDoc', 'id');
    }
  }

  @override
  CouchDBDoc rebuild(void Function(CouchDBDocBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CouchDBDocBuilder toBuilder() => new CouchDBDocBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CouchDBDoc &&
        id == other.id &&
        rev == other.rev &&
        deleted == other.deleted;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), rev.hashCode), deleted.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CouchDBDoc')
          ..add('id', id)
          ..add('rev', rev)
          ..add('deleted', deleted))
        .toString();
  }
}

class CouchDBDocBuilder
    implements Builder<CouchDBDoc, CouchDBDocBuilder>, CouchyBuilder {
  _$CouchDBDoc _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  CouchDBDocBuilder();

  CouchDBDocBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _rev = _$v.rev;
      _deleted = _$v.deleted;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant CouchDBDoc other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$CouchDBDoc;
  }

  @override
  void update(void Function(CouchDBDocBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CouchDBDoc build() {
    final _$result =
        _$v ?? new _$CouchDBDoc._(id: id, rev: rev, deleted: deleted);
    replace(_$result);
    return _$result;
  }
}

class _$PropDoc extends PropDoc {
  @override
  final BuiltMap<String, JsonObject> props;
  @override
  final String id;
  @override
  final String rev;
  @override
  final bool deleted;

  factory _$PropDoc([void Function(PropDocBuilder) updates]) =>
      (new PropDocBuilder()..update(updates)).build();

  _$PropDoc._({this.props, this.id, this.rev, this.deleted}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('PropDoc', 'id');
    }
  }

  @override
  PropDoc rebuild(void Function(PropDocBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PropDocBuilder toBuilder() => new PropDocBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PropDoc &&
        props == other.props &&
        id == other.id &&
        rev == other.rev &&
        deleted == other.deleted;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc($jc(0, props.hashCode), id.hashCode), rev.hashCode),
        deleted.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PropDoc')
          ..add('props', props)
          ..add('id', id)
          ..add('rev', rev)
          ..add('deleted', deleted))
        .toString();
  }
}

class PropDocBuilder
    implements Builder<PropDoc, PropDocBuilder>, PropsyBuilder {
  _$PropDoc _$v;

  MapBuilder<String, JsonObject> _props;
  MapBuilder<String, JsonObject> get props =>
      _$this._props ??= new MapBuilder<String, JsonObject>();
  set props(MapBuilder<String, JsonObject> props) => _$this._props = props;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  PropDocBuilder();

  PropDocBuilder get _$this {
    if (_$v != null) {
      _props = _$v.props?.toBuilder();
      _id = _$v.id;
      _rev = _$v.rev;
      _deleted = _$v.deleted;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant PropDoc other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PropDoc;
  }

  @override
  void update(void Function(PropDocBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PropDoc build() {
    _$PropDoc _$result;
    try {
      _$result = _$v ??
          new _$PropDoc._(
              props: _props?.build(), id: id, rev: rev, deleted: deleted);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'props';
        _props?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PropDoc', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
